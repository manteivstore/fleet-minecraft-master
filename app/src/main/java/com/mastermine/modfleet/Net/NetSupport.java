package com.mastermine.modfleet.Net;

import android.content.Context;
import android.os.Build;
import android.os.PowerManager;
import android.util.Log;

import com.mastermine.modfleet.BuildConfig;
import com.mastermine.modfleet.MyApplication;
import com.mastermine.modfleet.ads.AdsSuppost;
import com.mastermine.modfleet.bean.AdsObj;
import com.mastermine.modfleet.bean.ItemObj;
import com.mastermine.modfleet.bean.RequestObj;
import com.telpoo.frame.net.BaseNetSupport;
import com.telpoo.frame.net.NetConfig;
import com.telpoo.frame.object.BaseObject;
import com.telpoo.frame.utils.JsonSupport;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class NetSupport {
    private static final NetData simpleGet(String url) {
        BaseNetSupport baseNetSupport = BaseNetSupport.getInstance();
        NetConfig.Builder builder = new NetConfig.Builder();
        baseNetSupport.init(builder.build());

        String res = baseNetSupport.method_GET(url);
        Log.d("dataApi", "url: " + url);
        Log.d("dataApi", "Respone: " + res);
        NetData data = new NetData();
        if (res == null) {
            data.setConnectError();
            return data;
        }
        data.setSuccess(res);
        return data;
    }

    private static final NetData simplePost(String url, String parram, Context context) {
        BaseNetSupport baseNetSupport = BaseNetSupport.getInstance();
        NetConfig.Builder builder = new NetConfig.Builder();
        baseNetSupport.init(builder.build());

        String res = baseNetSupport.simplePostHttp(url, parram);
        Log.d("dataApi", "url: " + url);
        Log.d("dataApi", "Respone: " + res);
        NetData data = new NetData();
        if (res == null) {
            data.setConnectError();
            return data;
        }
        data.setSuccess(res);

        return data;
    }


    public static String addParramToUrl(String url, BaseObject object) {
        if (object == null) return url;

        return url + "?" + object.convert2NetParrams();
    }


    public static NetData getCategories() {
        NetData data = simpleGet(MyUrl.categories);
        if (data.getcode() != 1) return data;
        try {
            data.setSuccess(JsonSupport.jsonArray2BaseOj(data.getDataAsString()));
        } catch (Exception e) {
            e.printStackTrace();
            data.setCodeErrorServer();
            MyApplication.getInstance().trackException(e);
        }
        return data;
    }

    public static NetData getItems(BaseObject object) {
        NetData data = simpleGet(addParramToUrl(MyUrl.categories, object));
        if (data.getcode() != 1) return data;
        try {
            data.setSuccess(JsonSupport.jsonArray2BaseOj(data.getDataAsString()));
        } catch (Exception e) {
            e.printStackTrace();
            data.setCodeErrorServer();
            MyApplication.getInstance().trackException(e);
        }
        return data;
    }

    public static NetData getAds(Context context) {
        NetData data = simpleGet(MyUrl.ads + BuildConfig.APPLICATION_ID);

        if (data.getcode() != 1) return data;
        try {
            AdsSuppost.saveDataAds(new JSONObject(data.getDataAsString()), context);
            JSONObject jsonDatas = new JSONObject(data.getDataAsString());
            String list_ads = jsonDatas.optString("list_ads", "");
            if (!list_ads.isEmpty() || !list_ads.equals("null")) {
                JSONArray jsonarray = new JSONArray(list_ads);
                for (int i = 0; i < jsonarray.length(); i++) {
                    JSONObject jsonListAds = jsonarray.getJSONObject(i);
                    String type = jsonListAds.optString(AdsObj.type, "");
                    if (type.equals("admob")) {
                        SPRsupport.saveIdAppAdmob(context, jsonListAds.optString(AdsObj.app_id, ""));
                        // TODO: 8/2/20 ThaoTm dùng một list banner để nó cạnh tranh nhau về giá
                        SPRsupport.saveIdBannerAdmobList(context, jsonListAds.optString(AdsObj.banner_list, ""));
                        SPRsupport.saveIdFullAdmob(context, jsonListAds.optString(AdsObj.full, ""));
                        SPRsupport.saveIdVideoAdmob(context, jsonListAds.optString(AdsObj.video, ""));
                    } else {
                        SPRsupport.saveIdAppFan(context, jsonListAds.optString(AdsObj.app_id, ""));
                        SPRsupport.saveIdBannerFan(context, jsonListAds.optString(AdsObj.banner, ""));
                        SPRsupport.saveIdFullFan(context, jsonListAds.optString(AdsObj.full, ""));
                        SPRsupport.saveIdVideoFan(context, jsonListAds.optString(AdsObj.video, ""));
                    }
                }
            } else {
                SPRsupport.saveIdAppAdmob(context, jsonDatas.optString(AdsObj.app_id, ""));
                // TODO: 8/2/20 ThaoTm dùng một list banner để nó cạnh tranh nhau về giá
                SPRsupport.saveIdBannerAdmobList(context, jsonDatas.optString(AdsObj.banner_list, ""));
                SPRsupport.saveIdFullAdmob(context, jsonDatas.optString(AdsObj.full, ""));
                SPRsupport.saveIdVideoAdmob(context, jsonDatas.optString(AdsObj.video, ""));
            }
            SPRsupport.saveCountrySub(context, jsonDatas.optString(AdsObj.is_country_sub, ""));
            data.setSuccess("OK");
        } catch (Exception e) {
            e.printStackTrace();
            data.setCodeErrorServer();
            MyApplication.getInstance().trackException(e);
        }
        return data;
    }

    public static NetData getComment(BaseObject object) {
        NetData data = simpleGet(addParramToUrl(MyUrl.get_comment, object));
        if (data.getcode() != 1) return data;
        try {
            data.setSuccess(JsonSupport.jsonArray2BaseOj(data.getDataAsString()));
        } catch (Exception e) {
            e.printStackTrace();
            data.setCodeErrorServer();
            MyApplication.getInstance().trackException(e);
        }
        return data;
    }

    public static NetData postLike(BaseObject parramObject) {
        NetData data = CustomBaseNetSupport.getInstance().simplePostJson(MyUrl.like, parramObject);
        if (data.getcode() != 1) return data;
        String res = data.getDataAsString();
        try {
            BaseObject baseObject = JsonSupport.jsonObject2BaseOj(res);
            data.setSuccess(baseObject);
        } catch (JSONException e) {
            e.printStackTrace();
            data.setCodeErrorServer();
        }
        return data;
    }

    public static NetData postViewDownload(BaseObject parramObject) {
        NetData data = CustomBaseNetSupport.getInstance().simplePostJson(MyUrl.download, parramObject);
        if (data.getcode() != 1) return data;
        String res = data.getDataAsString();
        try {
            BaseObject baseObject = JsonSupport.jsonObject2BaseOj(res);
            data.setSuccess(baseObject);
        } catch (JSONException e) {
            e.printStackTrace();
            data.setCodeErrorServer();
        }
        return data;
    }

    public static NetData postComment(BaseObject parramObject) {
        NetData data = CustomBaseNetSupport.getInstance().simplePostJson(MyUrl.post_comment, parramObject);
        if (data.getcode() != 1) return data;
        String res = data.getDataAsString();
        try {
            BaseObject baseObject = JsonSupport.jsonObject2BaseOj(res);
            data.setSuccess(baseObject);
        } catch (Exception e) {
            e.printStackTrace();
            data.setCodeErrorServer();
        }
        return data;
    }

    public static NetData searchName(BaseObject parramObject) {
        NetData data = simpleGet(addParramToUrl(MyUrl.get_search, parramObject));
        if (data.getcode() != 1) return data;
        String res = data.getDataAsString();
        try {
            JSONArray jsonArray = new JSONArray(res);
            ArrayList<BaseObject> listSearch = JsonSupport.jsonArray2BaseOj(jsonArray);
            for (int i = 0; i < jsonArray.length(); i++) {
                BaseObject searchSpObj = new BaseObject();
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                searchSpObj.set(ItemObj.image, jsonObject.getString(ItemObj.image));
                searchSpObj.set(ItemObj.name, jsonObject.getString(ItemObj.name));
            }
            data.setSuccess(listSearch);
        } catch (JSONException e) {
            e.printStackTrace();
            data.setCodeErrorServer();
        }
        return data;
    }

    public static NetData smallList(BaseObject parramObject) {
        NetData data = simpleGet(MyUrl.small_list);
        if (data.getcode() != 1) return data;
        try {
            data.setSuccess(JsonSupport.jsonArray2BaseOj(data.getDataAsString()));
        } catch (Exception e) {
            e.printStackTrace();
            data.setCodeErrorServer();
            MyApplication.getInstance().trackException(e);
        }
        return data;
    }

    public static NetData getRequestDataV2(Context context) {
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        BaseObject pushObject = new BaseObject();
        pushObject.set(RequestObj.last_message_id, SPRsupport.getMsgId(context));
        pushObject.set(RequestObj.application_id, BuildConfig.APPLICATION_ID);
        pushObject.set(RequestObj.version_code, BuildConfig.VERSION_CODE);
        pushObject.set(RequestObj.country, context.getResources().getConfiguration().locale.getCountry().toUpperCase());
        pushObject.set(RequestObj.uid, SPRsupport.getImei(context));
        pushObject.set(RequestObj.name_phone, Build.MANUFACTURER);
        pushObject.set(RequestObj.model_phone, Build.MODEL);
        pushObject.set(RequestObj.version_phone, Build.VERSION.RELEASE);
        pushObject.set(RequestObj.id_token, SPRsupport.getToken(context));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            if (!pm.isInteractive()) pushObject.set(RequestObj.status_screen, "Off");
            else pushObject.set(RequestObj.status_screen, "On");
        }
        pushObject.set(RequestObj.last_time_request, SPRsupport.getTimeRequestLast(context));
        pushObject.set(RequestObj.last_link, SPRsupport.getLinkShow(context));
        pushObject.set(RequestObj.last_status, SPRsupport.getStatusRequestLast(context));
        Log.d("SonLv", "pushObject: " + pushObject.toJson());
        NetData data = CustomBaseNetSupport.getInstance().simplePostJson(MyUrl.request_v2, pushObject);
        if (data.getcode() != 1) return data;
        String res = data.getDataAsString();
        try {
            BaseObject baseObject = JsonSupport.jsonObject2BaseOj(res);
            data.setSuccess(baseObject);
        } catch (JSONException e) {
            e.printStackTrace();
            data.setCodeErrorServer();
        }
        return data;
    }
}