package com.mastermine.modfleet.onesignal;

import android.content.Context;
import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.mastermine.modfleet.BuildConfig;
import com.mastermine.modfleet.Net.SPRsupport;
import com.mastermine.modfleet.R;
import com.mastermine.modfleet.bean.OnesignalObj;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class EventFirebase {
    Context context;
    public static EventFirebase firebaseLoader;

    public static EventFirebase getInstall(Context context) {
        if (firebaseLoader == null) firebaseLoader = new EventFirebase(context);
        return firebaseLoader;
    }

    public EventFirebase(Context context) {
        this.context = context;
    }

    public void logEvent(String event) {
        DateFormat df = new SimpleDateFormat("dd MMM yyyy, HH:mm");
        Bundle params = new Bundle();
        params.putString(OnesignalObj.country, context.getResources().getConfiguration().locale.getCountry());
        params.putString(OnesignalObj.uid, SPRsupport.getIdOnesignal(context));
//        params.putString(OnesignalObj.link, SPRsupport.getLinkOnesignal(context));
        params.putString(OnesignalObj.time, df.format(Calendar.getInstance().getTime()));
        FirebaseAnalytics.getInstance(context).logEvent(event + context.getResources().getString(R.string.app_name) + BuildConfig.VERSION_CODE, params);
    }
}
