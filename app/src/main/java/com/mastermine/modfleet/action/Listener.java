package com.mastermine.modfleet.action;

import com.telpoo.frame.object.BaseObject;

/**
 * Created by Administrator on 4/12/2016.
 */
public class Listener {
    public interface OnItemClickStringListener {
        public void onItemClick(String value);
    }

    public interface OnItemClickIntListener {
        public void onItemClick(int value);
    }

    public interface OnInitTypeSimListener {
        public void onSuccess(boolean checkTest);
    }

    public interface OnKiemTraSoDuListener {
        public void onSuccess(String sodu);
    }

    public interface OnClickListener {
        public void onClick(int position);
    }
    public interface OnItemClickObjectListener {
        public void onItemClick(BaseObject object);
    }

    public interface OnClickBaseObjectListener {
        public void onClick(BaseObject object);
    }
    public interface OnClickStringListener {
        public void onClick(String value);
    }

    public interface OnDialogYesNoListener {
        public void onYesClick();
        public void onNoClick();
    }

    public interface OnPurchasesListener {
        public void onSuccess();
        public void onError();
    }

    public interface OnAdsLoadingStatusListener {
        public void onSuccess(boolean status);
    }

    public interface OnSpinListener {
        public void onWatchAds();
        public void onDownload();
    }

    public interface adsLoadFullVideo {
        void onAdLoaded(String type);

        void onLoadFailAds(String type);
    }


    public interface adsShowVideos {
        void onCloseAds(String type);

        void onLoadFailAds(String type);

        void onGetRewarded(String type);
    }

}
