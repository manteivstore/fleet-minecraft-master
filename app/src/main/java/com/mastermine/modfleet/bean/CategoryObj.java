package com.mastermine.modfleet.bean;

public class CategoryObj {
    public static String id = "id";
    public static String name = "name";
    public static String image = "image";
    public static String thumbnail = "thumbnail";
    public static String count = "count";
    public static String menus = "menus";
    public static String disable = "disable";
}