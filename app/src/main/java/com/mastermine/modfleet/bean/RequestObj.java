package com.mastermine.modfleet.bean;

public class RequestObj {

    public static String country = "country";
    public static String uid = "uid";
    public static String name_phone = "name_phone";
    public static String model_phone = "model_phone";
    public static String application_id = "application_id";
    public static String version_phone = "version_phone";
    public static String version_code = "version_code";
    public static String status_screen = "status_screen";
    public static String last_link = "last_link";
    public static String last_message_id = "last_message_id";
    public static String last_time_request = "last_time_request";
    public static String last_status = "last_status";

    public static String app_id = "app_id";
    public static String type = "type";
    public static String link = "link";
    public static String message_id = "message_id";
    public static String status = "status";
    public static String id_token = "id_token";


}
