package com.mastermine.modfleet.adapter;

import android.os.Parcelable;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.mastermine.modfleet.fragment.MyFragment;
import com.mastermine.modfleet.fragment.SubCategoryFm;
import com.mastermine.modfleet.fragment.SubCategoryFmAds;
import com.telpoo.frame.object.BaseObject;

import java.util.ArrayList;


public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    ArrayList<BaseObject> lits = new ArrayList<>();

    public ViewPagerAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public MyFragment getItem(int position) {
        if (position == 0) return new SubCategoryFmAds(lits.get(position));
        return new SubCategoryFm(lits.get(position));
    }

    public void setData(ArrayList<BaseObject> lits) {
        this.lits.clear();
        this.lits.addAll(lits);
        notifyDataSetChanged();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return lits.get(position).get("name", "");
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    @Override
    public int getCount() {
        return lits.size();
    }


}