package com.mastermine.modfleet.adapter;

import android.app.Activity;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mastermine.modfleet.R;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.mastermine.modfleet.MyApplication;
import com.mastermine.modfleet.CategorySubActivity;
import com.mastermine.modfleet.bean.CategoryObj;
import com.telpoo.frame.object.BaseObject;

import java.util.ArrayList;

public class CategoriesAdapterHolder extends RecyclerView.Adapter<CategoriesAdapterHolder.ViewHolder> {

    private ArrayList<BaseObject> mList = new ArrayList<>();
    Activity activity;
    LayoutInflater mInflay;

    public CategoriesAdapterHolder(Activity activity) {
        this.activity = activity;
        mInflay = LayoutInflater.from(activity);
    }

    public void setData(ArrayList<BaseObject> tmps) {
        mList.clear();
        for (int i = 0; i < tmps.size(); i++) {
            BaseObject item = tmps.get(i);
            if (!item.getBool(CategoryObj.disable, false)) mList.add(item);
        }
        notifyDataSetChanged();
    }

    public void addData(ArrayList<BaseObject> tmps) {
        mList.addAll(tmps);
        notifyDataSetChanged();
    }

    public BaseObject getItem(int i) {
        return mList.get(i);
    }

    public ArrayList<BaseObject> getAll() {
        return mList;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @SuppressWarnings("ResourceType")
    @Override
    public CategoriesAdapterHolder.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View sView = mInflay.inflate(R.layout.category, parent, false);
        return new CategoriesAdapterHolder.ViewHolder(sView);
    }

    @Override
    public void onBindViewHolder(final CategoriesAdapterHolder.ViewHolder holder, final int position) {
        BaseObject item = getItem(position);
        int res = -1;
        Log.d("ducqv", "item: " + item.toJson());
        switch (item.get(CategoryObj.id, "")) {
            case "mods":
                res = R.drawable.mods;
                break;
            case "maps":
                res = R.drawable.maps;
                break;
            case "textures":
                res = R.drawable.textures;
                break;
            case "servers":
                res = R.drawable.servers;
                break;
            case "seeds":
                res = R.drawable.seeds;
                break;
            case "builder":
                res = R.drawable.building;
                break;
            case "packs":
                res = R.drawable.packs;
                break;
        }

        if (res != -1) holder.imgAvatar.setImageResource(res);
        else
            ImageLoader.getInstance().displayImage(item.get(CategoryObj.image, ""), holder.imgAvatar, MyApplication.getImageOption());
        holder.tvCount.setText(item.get(CategoryObj.count, "coming soon 100+") + " " + item.get(CategoryObj.name, ""));
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgAvatar;
        TextView tvCount;

        public ViewHolder(View row) {
            super(row);
            tvCount = row.findViewById(R.id.tvCount);
            imgAvatar = row.findViewById(R.id.img);
            row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, CategorySubActivity.class);
                    intent.putExtra("data", getItem(getAdapterPosition()));
                    activity.startActivityForResult(intent, 1);
                }
            });
        }

    }


}
