package com.mastermine.modfleet.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.ads.doubleclick.PublisherAdView;
import com.mastermine.modfleet.DetailItemActivity;
import com.mastermine.modfleet.MyApplication;
import com.mastermine.modfleet.Net.SPRsupport;
import com.mastermine.modfleet.R;
import com.mastermine.modfleet.ads.AdsLoader;
import com.mastermine.modfleet.ads.listener;
import com.mastermine.modfleet.bean.CategoryObj;
import com.mastermine.modfleet.bean.ItemObj;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.telpoo.frame.object.BaseObject;

import java.util.ArrayList;
import java.util.Collections;

public class SmallIteamAdapter extends RecyclerView.Adapter<SmallIteamAdapter.ViewHolder> {
    private ArrayList<BaseObject> smallList = new ArrayList<>();
    Activity activity;
    LayoutInflater mInflay;
    boolean isCheck;
    int post_like = 0;

    public SmallIteamAdapter(Activity activity) {
        this.activity = activity;
        mInflay = LayoutInflater.from(activity);
    }

    public void setData(ArrayList<BaseObject> tmps) {
        smallList.clear();
        smallList.addAll(tmps);
        Collections.shuffle(smallList);
        notifyDataSetChanged();
    }

    public void addData(ArrayList<BaseObject> tmps) {
        smallList.clear();
        smallList.addAll(tmps);
        Collections.shuffle(smallList);
        notifyDataSetChanged();
    }

    public BaseObject getItem(int i) {
        return smallList.get(i);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        switch (viewType) {
            case 1:
                return new ViewHolder(mInflay.inflate(R.layout.item_ads_two, viewGroup, false));
            case 4:
                return new ViewHolder(mInflay.inflate(R.layout.item_ads_thre, viewGroup, false));
        }
        return new ViewHolder(mInflay.inflate(R.layout.item, viewGroup, false));
    }

    int check = -1;

    // TODO: 8/2/20 ThaoTm rên list này hiện ở 3 vị trí
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (position > check && holder.mPublisherAdView.getChildCount() == 0) {
            switch (position) {
                case 1:
                    AdsLoader.getInstall(activity).showBanner(holder.mPublisherAdView, 0, new listener.adsLoadShowBanner() {
                        @Override
                        public void onAdLoaded() {
                            holder.rlBanner.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onLoadFailAds() {
                            holder.rlBanner.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAdClosed() {
                            holder.rlBanner.setVisibility(View.GONE);
                        }
                    });
                    break;
                case 4:
                    AdsLoader.getInstall(activity).showBanner(holder.mPublisherAdView, 1, new listener.adsLoadShowBanner() {
                        @Override
                        public void onAdLoaded() {
                            holder.rlBanner.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onLoadFailAds() {
                            holder.rlBanner.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAdClosed() {
                            holder.rlBanner.setVisibility(View.GONE);
                        }
                    });
                    break;
            }
        }
        check++;

        BaseObject item = getItem(position);
        holder.tvName.setText(item.get(ItemObj.name, ""));
        holder.tvLike.setText(item.get(ItemObj.post_like, ""));
        holder.tvDownload.setText(item.get(ItemObj.download, ""));
        holder.tvView.setText(item.get(ItemObj.views, ""));

        isCheck = SPRsupport.isLike(item.get(ItemObj.id, ""), activity);
        post_like = Integer.parseInt(item.get(ItemObj.post_like, ""));
        if (isCheck) {
            post_like = post_like + 1;
            holder.tvLike.setText(post_like + "");
        }

        ImageLoader.getInstance().displayImage(item.get(ItemObj.thumbnail, ""),
                holder.imgAvatar, MyApplication.getImageOption(), new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        holder.layoutShimmer.startShimmer();
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        holder.layoutShimmer.stopShimmer();
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        holder.layoutShimmer.stopShimmer();
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                        holder.layoutShimmer.stopShimmer();
                    }
                });
    }

    @Override
    public int getItemCount() {
        if (smallList.size() > 10) return 10;
        else return smallList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgAvatar;
        TextView tvName;
        TextView tvLike;
        TextView tvComment;
        TextView tvDownload;
        TextView tvView;
        ShimmerFrameLayout layoutShimmer;
        PublisherAdView mPublisherAdView;
        private RelativeLayout rlBanner;

        public ViewHolder(View itemView) {
            super(itemView);
            imgAvatar = itemView.findViewById(R.id.img);
            tvName = itemView.findViewById(R.id.tvName);
            tvLike = itemView.findViewById(R.id.tvLike);
            tvComment = itemView.findViewById(R.id.tvComment);
            tvDownload = itemView.findViewById(R.id.tvDownload);
            tvView = itemView.findViewById(R.id.tvView);
            layoutShimmer = itemView.findViewById(R.id.layoutShimmer);
            mPublisherAdView = itemView.findViewById(R.id.mPublisherAdView);
            rlBanner = itemView.findViewById(R.id.rlBanner);
            rlBanner.setVisibility(View.GONE);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    BaseObject item = getItem(getAdapterPosition());
                    ImageLoader.getInstance().loadImage(item.get(CategoryObj.image, ""), null);
                    Intent intent = new Intent(activity, DetailItemActivity.class);
                    intent.putExtra("data", item);
                    intent.putExtra("type", "smalllisst");
                    intent.putExtra("category", (item.get(CategoryObj.name, "")));
                    AdsLoader.getInstall(activity).showFullPublisher(new listener.adsLoadShowFull() {
                        @Override
                        public void onAdLoaded(String type) {

                        }

                        @Override
                        public void onLoadFailAds(String type) {
                            activity.startActivityForResult(intent, 1);
                            activity.finish();
                        }

                        @Override
                        public void onAdClosed(String type) {
                            activity.startActivityForResult(intent, 1);
                            activity.finish();
                        }
                    });
                }
            });
        }
    }
}
