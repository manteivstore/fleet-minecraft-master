package com.mastermine.modfleet.adapter;

import android.app.Activity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mastermine.modfleet.R;
import com.mastermine.modfleet.bean.CommentObj;
import com.telpoo.frame.object.BaseObject;
import com.telpoo.frame.utils.JsonSupport;

import org.json.JSONException;

import java.util.ArrayList;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder> {

    private ArrayList<BaseObject> mListCmt = new ArrayList<>();
    Activity activity;
    LayoutInflater mInflay;
    ReplyCmtAdapter adapterReply;

    String post_id;

    public CommentAdapter(Activity activity) {
        this.activity = activity;
        mInflay = LayoutInflater.from(activity);
    }

    public void setData(ArrayList<BaseObject> tmps) {
        mListCmt.clear();
        for (int i = 0; i < tmps.size(); i++) {
            BaseObject item = tmps.get(i);
            mListCmt.add(item);
        }
        notifyDataSetChanged();
    }

    public void addData(ArrayList<BaseObject> tmps) {
        mListCmt.clear();
        mListCmt.addAll(tmps);
        notifyDataSetChanged();
    }

    public BaseObject getItem(int i) {
        return mListCmt.get(i);
    }

    public ArrayList<BaseObject> getAll() {
        return mListCmt;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_cmt, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        BaseObject itemCmt = getItem(position);
        post_id = itemCmt.get(CommentObj.post_id, "");
        holder.tvContent.setText(itemCmt.get(CommentObj.comment, ""));
        holder.tvTime.setText(itemCmt.get(CommentObj.comment_date, ""));
        try {
            adapterReply.setData(JsonSupport.jsonArray2BaseOj(itemCmt.get(CommentObj.childs, "")));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mListCmt.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvContent;
        TextView tvTime;
        RecyclerView rcItemComment;

        public ViewHolder(View itemView) {
            super(itemView);
            tvContent = itemView.findViewById(R.id.tvContent);
            tvTime = itemView.findViewById(R.id.tvTime);
            rcItemComment = itemView.findViewById(R.id.rcItemComment);

            adapterReply = new ReplyCmtAdapter(activity);
            rcItemComment.setHasFixedSize(true);
            rcItemComment.setItemAnimator(new DefaultItemAnimator());
            rcItemComment.setLayoutManager(new LinearLayoutManager(activity));
            rcItemComment.setAdapter(adapterReply);
        }
    }
}
