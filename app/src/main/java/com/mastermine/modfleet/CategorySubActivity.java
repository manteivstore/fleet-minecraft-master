package com.mastermine.modfleet;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;

import androidx.viewpager.widget.ViewPager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mastermine.modfleet.Net.TaskNet;
import com.mastermine.modfleet.adapter.ItemsAdapterHolder;
import com.mastermine.modfleet.adapter.ViewPagerAdapter;
import com.mastermine.modfleet.bean.CategoryObj;
import com.telpoo.frame.model.BaseModel;
import com.telpoo.frame.object.BaseObject;
import com.telpoo.frame.utils.JsonSupport;
import com.telpoo.frame.utils.KeyboardSupport;

import org.json.JSONException;

import java.util.ArrayList;

public class CategorySubActivity extends SwipeBackActivity implements View.OnClickListener {
    ViewPager viewPager;
    ViewPagerAdapter adapter;
    TabLayout tabLayout;
    ImageView imgGift;
    TextView tvType;
    TextView tvVersion;
    BaseObject object = new BaseObject();

    LinearLayout lnViewType;
    LinearLayout lnViewTextSearch;
    RelativeLayout rlBtSearch;
    ImageView imgBackSearch;
    ImageView imgCancelSearch;
    EditText edtSearch;
    RecyclerView rcListSearch;

    boolean isCheckSearch = false;
    String s = "";
    BaseObject objectRequest = new BaseObject();
    TaskNet taskNet;
    ItemsAdapterHolder searchAdapterHolder;
    ArrayList<BaseObject> list = new ArrayList<>();
    BaseModel baseModel = new BaseModel() {
        @Override
        public void onSuccess(int taskType, Object data, String msg) {
            super.onSuccess(taskType, data, msg);
            switch (taskType) {
                case TASK_SEARCH:
                    list = (ArrayList<BaseObject>) data;
                    searchAdapterHolder.setData(list);
                    break;
            }
        }

        @Override
        public void onFail(int taskType, String msg) {
            super.onFail(taskType, msg);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_category);
        initView();
        initData();

        imgGift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getInstance().trackEvent("ShowVongQuayMayMan", "", "");
            }
        });
    }

    public String getCategoryName() {
        return object.get(CategoryObj.name, "");
    }

    private void initView() {
        viewPager = findViewById(R.id.viewpager);
        tabLayout = findViewById(R.id.navigation);
        imgGift = findViewById(R.id.imgGift);
        tvType = findViewById(R.id.tvType);
        tvVersion = findViewById(R.id.tvVersion);

        lnViewType = findViewById(R.id.lnViewType);
        lnViewTextSearch = findViewById(R.id.lnViewTextSearch);
        rlBtSearch = findViewById(R.id.rlBtSearch);
        imgBackSearch = findViewById(R.id.imgBackSearch);
        imgCancelSearch = findViewById(R.id.imgCancelSearch);
        edtSearch = findViewById(R.id.edtSearch);
        rcListSearch = findViewById(R.id.rcListSearch);

        toolbar.setVisibility(View.VISIBLE);
        lnViewType.setVisibility(View.VISIBLE);
        rlBtSearch.setVisibility(View.VISIBLE);
        lnViewTextSearch.setVisibility(View.GONE);
        imgCancelSearch.setVisibility(View.GONE);
    }

    private void initData() {
        showProgressDialog(this);
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        object = getIntent().getParcelableExtra("data");
        tvType.setText(getCategoryName());
        tvVersion.setText(BuildConfig.VERSION_NAME);
        try {
            adapter.setData(JsonSupport.jsonArray2BaseOj(object.get(CategoryObj.menus, "")));
            tabLayout.setupWithViewPager(viewPager);
        } catch (JSONException e) {
            MyApplication.getInstance().trackException(e);
        }
        tvVersion.postDelayed(new Runnable() {
            @Override
            public void run() {
                closeProgressDialog();
            }
        }, 2000);
        rlBtSearch.setOnClickListener(this);
        imgBackSearch.setOnClickListener(this);
        imgCancelSearch.setOnClickListener(this);

        searchAdapterHolder = new ItemsAdapterHolder(this);
        rcListSearch.setHasFixedSize(true);
        rcListSearch.setItemAnimator(new DefaultItemAnimator());
        rcListSearch.setLayoutManager(new LinearLayoutManager(this));
        rcListSearch.setAdapter(searchAdapterHolder);
        rcListSearch.setOnTouchListener((view1, motionEvent) -> {
            switch (motionEvent.getAction()) {
                case MotionEvent.ACTION_DOWN:
                case MotionEvent.ACTION_POINTER_UP:
                case MotionEvent.ACTION_POINTER_DOWN:
                case MotionEvent.ACTION_MOVE:
                    KeyboardSupport.hideKeyboard(this, edtSearch);
                    break;
            }
            return false;
        });

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                s = editable.toString();
                if (!s.isEmpty()) {
                    objectRequest = new BaseObject();
                    objectRequest.set("s", s);
                    imgCancelSearch.setVisibility(View.VISIBLE);
                    rcListSearch.setVisibility(View.VISIBLE);
                } else {
                    imgCancelSearch.setVisibility(View.GONE);
                    rcListSearch.setVisibility(View.GONE);
                }
                loadSearch();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        AdmobLoader.getInstall(this).showFull();
        tvVersion.setText(BuildConfig.VERSION_NAME);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rlBtSearch:
                isCheckSearch(isCheckSearch);
                isCheckSearch = !isCheckSearch;
                break;
            case R.id.imgBackSearch:
                isCheckSearch(isCheckSearch);
                KeyboardSupport.hideKeyboard(this, edtSearch);
                isCheckSearch = !isCheckSearch;
                break;
            case R.id.imgCancelSearch:
                edtSearch.setText("");
                break;
        }
    }

    private void isCheckSearch(boolean isCheck) {
        if (!isCheck) {
            lnViewType.setVisibility(View.GONE);
            rlBtSearch.setVisibility(View.GONE);
            lnViewTextSearch.setVisibility(View.VISIBLE);
            edtSearch.requestFocus();
            KeyboardSupport.showKeyboard(this, edtSearch);
        } else {
            lnViewType.setVisibility(View.VISIBLE);
            rlBtSearch.setVisibility(View.VISIBLE);
            lnViewTextSearch.setVisibility(View.GONE);
            edtSearch.setText("");
        }
    }

    public void loadSearch() {
        taskNet = new TaskNet(baseModel, TASK_SEARCH, this);
        taskNet.setTaskParram("parram", objectRequest);
        taskNet.exe();
    }
}
