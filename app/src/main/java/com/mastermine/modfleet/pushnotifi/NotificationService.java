package com.mastermine.modfleet.pushnotifi;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.mastermine.modfleet.BuildConfig;
import com.mastermine.modfleet.DetailItemActivity;
import com.mastermine.modfleet.Net.SPRsupport;
import com.mastermine.modfleet.R;
import com.mastermine.modfleet.bean.ItemObj;
import com.onesignal.NotificationExtenderService;
import com.onesignal.OSNotificationReceivedResult;
import com.telpoo.frame.object.BaseObject;

import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class NotificationService extends NotificationExtenderService {

    @Override
    protected boolean onNotificationProcessing(OSNotificationReceivedResult content) {
        Log.d("ducqv", "content: " + content.payload.additionalData);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(BuildConfig.APPLICATION_ID, name, importance);
            channel.setDescription(description);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
        Intent intent = new Intent(this, DetailItemActivity.class);
        URL url;
        HttpURLConnection connection;
        InputStream input;
        Bitmap myBitmap = null;
        String nametype = "";
        String description = "";
        try {
            JSONObject jsonObject = content.payload.additionalData;
            BaseObject object = new BaseObject();
            description = jsonObject.getString(ItemObj.description);
            nametype = jsonObject.getString(ItemObj.name);

            object.set(ItemObj.image, jsonObject.getString(ItemObj.image));
            object.set(ItemObj.download, jsonObject.getString(ItemObj.download));
            object.set(ItemObj.thumbnail, jsonObject.getString(ItemObj.thumbnail));
            object.set(ItemObj.file, jsonObject.getString(ItemObj.file));
            object.set(ItemObj.post_id, jsonObject.getString(ItemObj.post_id));
            object.set(ItemObj.seed, jsonObject.getString(ItemObj.seed));
            object.set(ItemObj.name, nametype);
            object.set(ItemObj.description, description);
            object.set(ItemObj.post_like, jsonObject.getString(ItemObj.post_like));
            object.set(ItemObj.version, jsonObject.getString(ItemObj.version));
            object.set(ItemObj.views, jsonObject.getString(ItemObj.views));
            object.set(ItemObj.mcversion, jsonObject.getString(ItemObj.mcversion));
            Bundle bundle = new Bundle();
            bundle.putParcelable("data", object);
            bundle.putString("type", "notifi");
            intent.putExtras(bundle);

            url = new URL(jsonObject.getString(ItemObj.thumbnail));
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            input = connection.getInputStream();
            myBitmap = BitmapFactory.decodeStream(input);
        } catch (Exception e) {
            e.printStackTrace();
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, BuildConfig.APPLICATION_ID)
                .setSmallIcon(R.drawable.ic_120)
                .setContentTitle("A GREAT NEW MOD - " + nametype)
                .setStyle(new NotificationCompat.BigPictureStyle().bigPicture(myBitmap))
                .setContentText(description)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(0, builder.build());
        SimpleDateFormat dateformat = new SimpleDateFormat("dd-MM-yyyy");
        try {
            long current = dateformat.parse(dateformat.format(new Date())).getTime();
            SPRsupport.saveTimeCheck(this, current);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return true;
    }

    public static boolean enableShowAds(Context context) {
        SimpleDateFormat dateformat = new SimpleDateFormat("dd-MM-yyyy");
        try {
            long current = dateformat.parse(dateformat.format(new Date())).getTime();
            long getTime = SPRsupport.getTimeCheck(context);
            long diff = current - getTime;
            long dayCount = diff / (24 * 60 * 60 * 1000);
            if (dayCount == 0) return false;
            else return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
