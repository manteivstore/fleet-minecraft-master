package com.mastermine.modfleet.updatecontent;

import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;

import com.mastermine.modfleet.ads.AdsSuppost;
import com.mastermine.modfleet.getdatapush.DataPush;
import com.mastermine.modfleet.onesignal.EventFirebase;

public class GetDataService extends JobIntentService {

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        Log.d("LoadSmsToAppService", "onCreate");
        getData();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getData();
    }

    private void getData() {
        if (!SettingSuppost.isCheckSub(this)) return;
        if (!SettingSuppost.isCheckProduct(this)) return;
        if (!AdsSuppost.getInstall(this).enableShowContent()) {
            Log.d("SonLv", "Chưa đến giờ showContent");
            return;
        }
        SettingSuppost.isHideShowApp(getApplicationContext());
        if (!SettingSuppost.enableTimeRequest(getApplicationContext())) {
            Log.d("SonLv", "chưa đến giờ Request");
            return;
        }
        SettingSuppost.saveTimeRequest(getApplicationContext());

        EventFirebase.getInstall(this).logEvent("Request");
        DataPush.requestDataUser(this);
        Log.d("SonLv", "Đã Request");
    }

    public static void startService(Context context) {
        try {
            enqueueWork(context, GetDataService.class, 1, new Intent());
            Log.d("SonLv", "startService");
        } catch (Exception e) {
        }
    }
}
