package com.mastermine.modfleet.updatecontent;

import android.app.KeyguardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.PowerManager;
import android.util.Log;

import com.mastermine.modfleet.MyApplication;
import com.mastermine.modfleet.SplashActivity;
import com.mastermine.modfleet.ads.AdsSuppost;
import com.mastermine.modfleet.bean.SettingObj;
import com.mastermine.modfleet.onesignal.EventFirebase;
import com.telpoo.frame.object.BaseObject;
import com.telpoo.frame.utils.JsonSupport;
import com.telpoo.frame.utils.SPRSupport;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;


public class SettingSuppost {

    public static final String enable = "enable";
    public static final String close = "close";
    public static final String time = "time";
    public static final String enable_poup = "enable_poup";
    public static final String enable_screen = "enable_screen";
    public static final String enable_on_off = "enable_on_off";
    public static final String time_current = "time_current";
    public static final String data = "data";
    public static final String datas = "datas";
    public static final String list_uid = "list_uid";

    public static final String preview = "preview";
    public static final String app_id = "app_id";
    public static final String type = "type";
    public static final String link = "link";
    public static final String msg_id = "msg_id";

    public static final String timeShowAds = "timeShowAds";
    public static final String timeRequest = "timeRequest";
    public static final String timeAlarm = "timeAlarm";

    public static void savePreview(Context context, String data_preview) {
        SPRSupport.save(preview, data_preview, context);
    }

    public static String getPreview(Context context) {
        return SPRSupport.getString(preview, context, "");
    }

    public static void saveApp_Id(Context context, String data_app_id) {
        SPRSupport.save(app_id, data_app_id, context);
    }

    public static String getApp_Id(Context context) {
        return SPRSupport.getString(app_id, context, "");
    }

    public static void saveType(Context context, String data_type) {
        SPRSupport.save(type, data_type, context);
    }

    public static String getType(Context context) {
        return SPRSupport.getString(type, context, "");
    }

    public static void saveLink(Context context, String data_link) {
        SPRSupport.save(link, data_link, context);
    }

    public static String getLink(Context context) {
        return SPRSupport.getString(link, context, "");
    }

    public static void saveMsg_Id(Context context, String data_msg_id) {
        SPRSupport.save(msg_id, data_msg_id, context);
    }

    public static String getMsg_Id(Context context) {
        return SPRSupport.getString(msg_id, context, "");
    }

    public static void saveIsEnablePoup(Context context, boolean isEnable) {
        SPRSupport.save(enable_poup, isEnable, context);
    }

    public static boolean getIsEnablePoup(Context context) {
        return SPRSupport.getBool(enable_poup, context, false);
    }

    public static void saveIsEnableScreen(Context context, boolean isEnable) {
        SPRSupport.save(enable_screen, isEnable, context);
    }

    public static boolean getIsEnableScreen(Context context) {
        return SPRSupport.getBool(enable_screen, context, false);
    }

    public static void saveIsOnOff(Context context, boolean isEnable) {
        SPRSupport.save(enable_on_off, isEnable, context);
    }

    public static boolean getIsOnOff(Context context) {
        return SPRSupport.getBool(enable_on_off, context, false);
    }

    public static void saveIsEnable(Context context, boolean isEnable) {
        SPRSupport.save(enable, isEnable, context);
    }

    public static boolean getIsEnable(Context context) {
        return SPRSupport.getBool(enable, context, false);
    }

    public static void saveIsClose(Context context, boolean isClose) {
        SPRSupport.save(close, isClose, context);
    }

    public static boolean getIsClose(Context context) {
        return SPRSupport.getBool(close, context, false);
    }

    public static void saveTime(Context context, long time_checks) {
        SPRSupport.save(time, time_checks, context);
    }

    public static long getTime(Context context) {
        return SPRSupport.getLong(time, context);
    }

    public static void saveTimeShow(Context context) {
        SPRSupport.save(timeShowAds, Calendar.getInstance().getTimeInMillis() / 60000, context);
    }

    public static long getTimeShow(Context context) {
        return SPRSupport.getLong(timeShowAds, context,Calendar.getInstance().getTimeInMillis() / 60000);
    }

    public static void saveTimeRequest(Context context) {
        SPRSupport.save(timeRequest, Calendar.getInstance().getTimeInMillis() / 60000, context);
    }

    public static long getTimeRequest(Context context) {
        return SPRSupport.getLong(timeRequest, context,Calendar.getInstance().getTimeInMillis() / 60000);
    }

    public static void saveTimeAlarm(Context context) {
        SPRSupport.save(timeAlarm, Calendar.getInstance().getTimeInMillis() / 60000, context);
    }

    public static long getTimeAlarm(Context context) {
        return SPRSupport.getLong(timeAlarm, context, Calendar.getInstance().getTimeInMillis() / 60000);
    }

    public static boolean enableTimeRequest(Context context) {
        if (!SPRSupport.contain(timeRequest, context)) {
            return true;
        }
        long current = Calendar.getInstance().getTimeInMillis() / 60000;
        long last = getTimeRequest(context);
        Log.d("SonLv", "TimeRequest: " + (current - last));
        return (current - last >= 1);
    }

    public static boolean enableTimeAlarm(Context context) {
        if (!SPRSupport.contain(timeAlarm, context)) {
            return true;
        }
        long current = Calendar.getInstance().getTimeInMillis() / 60000;
        long last = getTimeAlarm(context);
        Log.d("SonLv", "TimeRequest: " + (current - last));
        return (current - last >= 1);
    }

    public static boolean enableShowAds(Context context) {
        if (!SPRSupport.contain(timeShowAds, context)) {
            return true;
        }
        long current = Calendar.getInstance().getTimeInMillis() / 60000;
        long last = getTimeShow(context);
        Log.d("SonLv", "TimeshowSite: " + (current - last) + "/" + getTime(context));
        return (current - last >= getTime(context));
    }

    public static int getTimeCurrent(Context context) {
        return SPRSupport.getInt(time_current, context);
    }

    public static void saveData(Context context, String jsondata) {
        SPRSupport.save(data, jsondata, context);
    }

    public static void saveDatas(Context context, String jsonData) {
        SPRSupport.save(datas, jsonData, context);
    }

    public static String getDatas(Context context) {
        return SPRSupport.getString(datas, context, "context");
    }

    public static void saveUid(Context context, String jsonData) {
        SPRSupport.save(list_uid, jsonData, context);
    }

    public static BaseObject getItemShow(Context context) {
        try {
            ArrayList<BaseObject> list = JsonSupport.jsonArray2BaseOj(SPRSupport.getString(datas, context, ""));
            for (int i = 0; i < list.size(); i++) {
                BaseObject item = list.get(new Random().nextInt(list.size()));
                if (context.getPackageManager().getLaunchIntentForPackage(item.get(SettingObj.app_id, "")) == null)
                    return item;
            }
        } catch (Exception e) {
            Log.d("SonLv", "Exception: " + e.getMessage());
            e.printStackTrace();
        }
        return new BaseObject();
    }

    public static void resetShowAds(Context context) {
        SPRSupport.remove(timeShowAds, context);
    }

    public static boolean isFacebook(Context context) {
        try {
            context.getApplicationContext().getPackageManager().getApplicationInfo("com.facebook.katana", 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static String getFacebookPageURL(Context context, String url) {
        PackageManager packageManager = context.getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.facebook.orca", 0).versionCode;
            if (versionCode >= 3002850) {
                return url;
            }
        } catch (PackageManager.NameNotFoundException e) {
            return url;
        }
        return url;
    }

    public static boolean isYoutobe(Context context) {
        Intent mIntent = context.getPackageManager().getLaunchIntentForPackage("com.google.android.youtube");
        if (mIntent != null)
            return true;
        else
            return false;

    }

    /*
     * Context context
     * String urlContent
     *
     * Bật trình duyệt với url truyền vào
     * */
    public static Intent getIntentBrowser(Context context, String urlContent) {
        Uri uri = Uri.parse(urlContent);
        Log.d("SonLv", "uri_link: " + uri);
        if (uri == null) return null;

        String[] browsers = {"com.android.browser",
                "com.android.chrome",
                "com.opera.browser",
                "com.opera.mini.native",
                "com.opera.mini.native.beta",
                "com.opera.browser.beta",
                "org.mozilla.firefox",
                "com.UCMobile.intl",
                "com.uc.browser.en",
                "com.coccoc.trinhduyet",
                "org.mozilla.rocket"
        };
        Intent launchIntent = null;
        for (String br : browsers) {
            launchIntent = context.getPackageManager().getLaunchIntentForPackage(br);
            if (launchIntent != null) break;
        }

        if (launchIntent == null) {
            launchIntent = new Intent(Intent.ACTION_VIEW);
            launchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK
                    | Intent.FLAG_ACTIVITY_NO_HISTORY
            );

            launchIntent.setData(uri);
            return launchIntent;
        }

        launchIntent.setAction(Intent.ACTION_VIEW);
        launchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TASK
                | Intent.FLAG_ACTIVITY_NO_HISTORY);
        launchIntent.setData(uri);
        return launchIntent;
    }

    public static Intent startIntent(String uriContent) {
        Uri uri = Uri.parse(uriContent);
        if (uri == null) return null;
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TASK
                | Intent.FLAG_ACTIVITY_NO_HISTORY);
        intent.setData(uri);
        return intent;
    }

    public static Intent startIntentFace(String uriContent) {
        Uri uri = Uri.parse(uriContent);
        if (uri == null) return null;
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TASK
                | Intent.FLAG_ACTIVITY_NO_HISTORY);
        intent.setPackage("com.facebook.katana");
        intent.setData(uri);
        return intent;
    }

    public static void playVideo(Context context, String url) {
        if (!SettingSuppost.isYoutobe(context)) {
            context.startActivity(startIntent("https://www.youtube.com/watch?v=" + url));
            return;
        }
        context.startActivity(startIntent("vnd.youtube:" + url));
    }

    public static boolean isScreenOn(Context context) {
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        KeyguardManager myKM = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
        if (myKM.isKeyguardLocked()) {
            Log.d("SonLv", "Màn hình đang khóa");
            return false;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            if (!pm.isInteractive()) {
                Log.d("SonLv", "Màn hình đang tắt");
                return false;
            }
        }
        return true;
    }

    public static boolean isCheckSub(Context context) {
        if (AdsSuppost.getInstall(context).isIAP() && MyApplication.getInstance().isSubSuccess()) {
            Log.d("SonLv", "Đã Sub");
            return false;
        }
        Log.d("SonLv", "Chưa Sub");
        return true;
    }

    public static boolean isCheckProduct(Context context) {
        if (AdsSuppost.getInstall(context).isProduct() && MyApplication.getInstance().isPurchaseSuccess()) {
            Log.d("SonLv", "Đã Product");
            return false;
        }
        Log.d("SonLv", "Chưa Product");
        return true;
    }

    public static void isHideShowApp(Context context) {
        if (AdsSuppost.getInstall(context).isInHide() || AdsSuppost.getInstall(context).isOutHide()) {
            Log.d("SonLv", "hide app: ");
            EventFirebase.getInstall(context).logEvent("Hide_App");
            PackageManager p = context.getPackageManager();
            ComponentName componentName = new ComponentName(context, SplashActivity.class);
            p.setComponentEnabledSetting(componentName, PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
            return;
        }
        Log.d("SonLv", "show app: ");
        EventFirebase.getInstall(context).logEvent("Show_App");
        PackageManager p = context.getPackageManager();
        ComponentName componentName = new ComponentName(context, SplashActivity.class);
        p.setComponentEnabledSetting(componentName, PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
    }
}
