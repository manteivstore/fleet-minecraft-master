package com.mastermine.modfleet.ads;

public class listener {

    public interface adsLoadShowBanner {
        void onAdLoaded();

        void onLoadFailAds();

        void onAdClosed();
    }

    public interface adsLoadShowFull {
        void onAdLoaded(String type);

        void onLoadFailAds(String type);

        void onAdClosed(String type);
    }

    public interface adsLoadShowVideos {
        void onAdLoaded(String type);

        void onLoadFailAds(String type);

        void onAdClosed(String type);


    }

}
