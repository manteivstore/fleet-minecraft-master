package com.mastermine.modfleet.ads;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.doubleclick.PublisherAdView;
import com.google.android.gms.ads.doubleclick.PublisherInterstitialAd;
import com.mastermine.modfleet.Net.SPRsupport;
import com.mastermine.modfleet.Net.TaskNet;
import com.mastermine.modfleet.Net.TaskType;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.google.firebase.analytics.FirebaseAnalytics;

import com.mastermine.modfleet.R;
import com.telpoo.frame.model.BaseModel;

public class AdsLoader {
    public static String TASK_ADMOB_ADS = "TASK_ADMOB_ADS";

    Context context;
    public static AdsLoader adsLoader;
    private PublisherInterstitialAd mPublisherInterstitialAd;
    private RewardedVideoAd mRewardedVideoAd;

    boolean isCheckRewardedVideoAd;

    public static AdsLoader getInstall(Context context) {
        if (adsLoader == null) adsLoader = new AdsLoader(context);
        return adsLoader;
    }

    public AdsLoader(Context context) {
        this.context = context;
    }

    public void initSDK(Context context) {
        MobileAds.initialize(context);
    }

    public void initAds() {
        TaskNet net = new TaskNet(new BaseModel() {
            @Override
            public void onSuccess(int taskType, Object data, String msg) {
                super.onSuccess(taskType, data, msg);
                Log.d("DucQv", "onSuccess Ads: ");
                loadAdsFullPublisher();
                loadVideoAdsmob();
            }

            @Override
            public void onFail(int taskType, String msg) {
                super.onFail(taskType, msg);
                Log.d("DucQv", "onFail: " + msg);
            }
        }, TaskType.TASK_ADS, context);
        net.exe();
    }

    private PublisherAdRequest getPublisherAdRequest() {
        return new PublisherAdRequest.Builder().addTestDevice("C2D11B3358FB9752A8ED9DFC414035A0").build();
    }

    public void requestBanner(PublisherAdView mPublisherAdView, final listener.adsLoadShowBanner loadShowBaner) {
        mPublisherAdView.loadAd(getPublisherAdRequest());
        mPublisherAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                loadShowBaner.onAdLoaded();
            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                loadShowBaner.onLoadFailAds();
            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();
                loadShowBaner.onAdClosed();
            }
        });
    }

    public void showBanner(ViewGroup viewGroup, int index, final listener.adsLoadShowBanner loadShowBaner) {
        final String id = SPRsupport.getIdBannerAdmobRandomList(context, index);
        Log.d("DucQv", index + " id Banner: " + id);
        if (id.isEmpty()) {
            Log.d("DucQv", "Id banner empty");
            return;
        }
        PublisherAdView adView = new PublisherAdView(context);
        adView.setAdSizes(AdSize.BANNER, AdSize.LARGE_BANNER, AdSize.MEDIUM_RECTANGLE);
        adView.setAdUnitId(id);
        adView.loadAd(getPublisherAdRequest());
        viewGroup.addView(adView);
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                loadShowBaner.onAdLoaded();
                Log.d("DucQv", "onAdLoaded: ");
            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                loadShowBaner.onLoadFailAds();
                Log.d("DucQv", "onAdFailedToLoad: ");
            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();
                loadShowBaner.onAdClosed();
                Log.d("DucQv", "onAdClosed: ");
            }
        });
        Log.d("DucQv", "showBanner");
    }

    public void showBannerLarge(ViewGroup viewGroup, int index) {
        if (!AdsSuppost.getInstall(context).isAds()) return;
        final String id = SPRsupport.getIdBannerAdmobRandomList(context, index);
        Log.d("DucQv", index + " id Banner: " + id);
        if (id.isEmpty()) {
            Log.d("DucQv", "Id banner empty");
            return;
        }
        PublisherAdView adView = new PublisherAdView(context);
        adView.setAdSizes(AdSize.BANNER, AdSize.LARGE_BANNER);
        adView.setAdUnitId(id);
        adView.loadAd(getPublisherAdRequest());
        viewGroup.addView(adView);
        Log.d("DucQv", "showBanner");
    }

    public void loadAdsFullPublisher() {
        if (!AdsSuppost.getInstall(context).isAds()) return;
        String country = context.getResources().getConfiguration().locale.getCountry();
        if (country.equals(SPRsupport.getCountrySub(context))) {
            Log.d("DucQv", "Dừng country " + SPRsupport.getCountrySub(context));
            return;
        }

        String id = SPRsupport.getIdFullAdmob(context);
        Log.d("DucQv", "id_Admob_Full: " + id);
        if (id.isEmpty()) {
            Log.d("DucQv", "Id full empty");
            return;
        }
        mPublisherInterstitialAd = new PublisherInterstitialAd(context);
        mPublisherInterstitialAd.setAdUnitId(id);
        mPublisherInterstitialAd.loadAd(getPublisherAdRequest());
        mPublisherInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                Log.d("DucQv", "onAdLoaded_Full: ");
            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                Log.d("DucQv", "onAdFailedToLoad_Full: ");
            }
        });
        Log.d("DucQv", "loadAdsFull");
    }

    public void showFullPublisher(final listener.adsLoadShowFull adsLoadShowFull) {
        if (!AdsSuppost.getInstall(context).enableShowAds()) {
            adsLoadShowFull.onLoadFailAds("Publisher");
            return;
        }
        if (mPublisherInterstitialAd == null) {
            loadAdsFullPublisher();
            adsLoadShowFull.onLoadFailAds("Publisher");
            return;
        }
        if (mPublisherInterstitialAd.isLoaded())
            mPublisherInterstitialAd.show();
        else {
            adsLoadShowFull.onLoadFailAds("Publisher");
            loadAdsFullPublisher();
        }

        mPublisherInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                adsLoadShowFull.onAdClosed("Publisher");
                AdsSuppost.getInstall(context).saveTimeShowAds();
                loadAdsFullPublisher();
            }

        });
    }


    public void loadVideoAdsmob() {
        if (!AdsSuppost.getInstall(context).isAds()) return;
        String id = SPRsupport.getIdVideoAdmob(context);
        Log.d("Ducqv", "id_VideoAdsmob: " + id);
        if (id.isEmpty()) {
            return;
        }
        mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(context);
        mRewardedVideoAd.loadAd(id, AdsSuppost.getInstall(context).getAdRequest());
    }

    public void showVideoAdmob(listener.adsLoadShowVideos adsLoadShowVideos) {
        if (mRewardedVideoAd == null) {
            adsLoadShowVideos.onLoadFailAds(TASK_ADMOB_ADS);
            loadVideoAdsmob();
            return;
        }
        if (mRewardedVideoAd.isLoaded()) {
            Log.d("Ducqv", "showVideoAdmob_LoadTruoc: ");
            mRewardedVideoAd.show();
            mRewardedVideoAd.setRewardedVideoAdListener(new RewardedVideoAdListener() {
                @Override
                public void onRewardedVideoAdLoaded() {
                }

                @Override
                public void onRewardedVideoAdOpened() {
                }

                @Override
                public void onRewardedVideoStarted() {
                    isCheckRewardedVideoAd = false;
                }

                @Override
                public void onRewardedVideoAdClosed() {
                    loadVideoAdsmob();
                    if (!isCheckRewardedVideoAd) {
                        Log.d("Ducqv", "Không xem hết video");
                        return;
                    }
                    adsLoadShowVideos.onAdClosed(TASK_ADMOB_ADS);
                }

                @Override
                public void onRewarded(RewardItem rewardItem) {
                    isCheckRewardedVideoAd = true;
                }

                @Override
                public void onRewardedVideoAdLeftApplication() {
                }

                @Override
                public void onRewardedVideoAdFailedToLoad(int i) {
                }

                @Override
                public void onRewardedVideoCompleted() {
                }
            });
            FirebaseAnalytics.getInstance(context).logEvent("show_Video_Admob", new Bundle());
            return;
        }
        adsLoadShowVideos.onLoadFailAds(TASK_ADMOB_ADS);
        loadVideoAdsmob();
    }

    private static void showDialogAds(Context context) {
        LayoutInflater myInflater = LayoutInflater.from(context);
        View view = myInflater.inflate(R.layout.dialog_ads, null);
        Toast mytoast = new Toast(context);
        mytoast.setView(view);
        mytoast.setDuration(Toast.LENGTH_SHORT);
        mytoast.show();
    }
}
