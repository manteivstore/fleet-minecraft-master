package com.mastermine.modfleet;

import android.content.Intent;

import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.doubleclick.PublisherAdView;
import com.mastermine.modfleet.Net.TaskNet;
import com.mastermine.modfleet.Net.TaskType;
import com.mastermine.modfleet.action.DialogsSupport;
import com.mastermine.modfleet.action.Listener;
import com.mastermine.modfleet.adapter.CategoriesAdapterHolder;
import com.mastermine.modfleet.adapter.SlidePagerAdapter;
import com.mastermine.modfleet.ads.AdsLoader;
import com.mastermine.modfleet.ads.listener;
import com.mastermine.modfleet.bean.CategoryObj;
import com.mastermine.modfleet.onesignal.EventFirebase;
import com.mastermine.modfleet.updatecontent.SettingSuppost;
import com.telpoo.frame.model.BaseModel;
import com.telpoo.frame.net.BaseNetSupport;
import com.telpoo.frame.object.BaseObject;

import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;

public class MainActivity extends SwipeBackActivity implements TaskType, View.OnClickListener {
    private ViewPager viewPager;
    private CircleIndicator circleIndicator;
    private SlidePagerAdapter myPager;

    CategoriesAdapterHolder adapter;
    RecyclerView mRecyclerView;
    View layoutSplash;
    TextView tvToolBar;
    TextView tvVersion;
    ImageView imgGift;
    ArrayList<BaseObject> datas;
    BaseObject item;

    ImageView imgMaps;
    ImageView imgBuilDing;
    ImageView imgMods;
    ImageView imgSeeds;
    ImageView imgServers;
    ImageView imgPacks;
    ImageView imgTextures;

    String type = "";
    private PublisherAdView mPublisherAdView;
    private RelativeLayout rlBanner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        initData();
        EventFirebase.getInstall(this).logEvent("intall");
    }

    private void initView() {
        layoutSplash = findViewById(R.id.layoutSplash);
        tvToolBar = findViewById(R.id.tvToolBar);
        tvVersion = findViewById(R.id.tvVersion);
        imgGift = findViewById(R.id.imgGift);

        viewPager = findViewById(R.id.view_pager);
        circleIndicator = findViewById(R.id.circle);
        mRecyclerView = findViewById(R.id.list);

        imgMaps = findViewById(R.id.imgMaps);
        imgBuilDing = findViewById(R.id.imgBuilDing);
        imgMods = findViewById(R.id.imgMods);
        imgSeeds = findViewById(R.id.imgSeeds);
        imgServers = findViewById(R.id.imgServers);
        imgPacks = findViewById(R.id.imgPacks);
        imgTextures = findViewById(R.id.imgTextures);

        mPublisherAdView = findViewById(R.id.mPublisherAdView);
        rlBanner = findViewById(R.id.rlBanner);
        rlBanner.setVisibility(View.GONE);
        AdsLoader.getInstall(this).showBanner(mPublisherAdView, 0, new listener.adsLoadShowBanner() {
            @Override
            public void onAdLoaded() {
                rlBanner.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadFailAds() {
                rlBanner.setVisibility(View.GONE);
            }

            @Override
            public void onAdClosed() {
                rlBanner.setVisibility(View.GONE);
            }
        });
    }

    private void initData() {
        showProgressDialog(this);
        layoutSplash.setVisibility(View.VISIBLE);
        myPager = new SlidePagerAdapter(this);
        viewPager.setAdapter(myPager);
        circleIndicator.setViewPager(viewPager);
        adapter = new CategoriesAdapterHolder(this);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(adapter);

        TaskNet net = new TaskNet(new BaseModel() {
            @Override
            public void onSuccess(int taskType, Object data, String msg) {
                super.onSuccess(taskType, data, msg);
                adapter.setData((ArrayList<BaseObject>) data);
                datas = (ArrayList<BaseObject>) data;
                layoutSplash.setVisibility(View.GONE);
                closeProgressDialog();
            }

            @Override
            public void onFail(int taskType, String msg) {
                super.onFail(taskType, msg);
                closeProgressDialog();
                Toast.makeText(MainActivity.this, msg, Toast.LENGTH_LONG).show();
                if (!BaseNetSupport.isNetworkAvailable(MainActivity.this))
                    DialogsSupport.showDialogYes(MainActivity.this, "Clode", "Network not connected!", null, new Listener.OnDialogYesNoListener() {
                        @Override
                        public void onYesClick() {
                            finish();
                        }

                        @Override
                        public void onNoClick() {

                        }
                    });
            }
        }, TASK_CATEGORIES, this);
        net.exe();

        tvToolBar.setText(getResources().getText(R.string.app_name));
        tvVersion.setText(BuildConfig.VERSION_NAME);
        MyApplication.getInstance().trackScreenView("MainActivity");

        imgMaps.setOnClickListener(this);
        imgBuilDing.setOnClickListener(this);
        imgMods.setOnClickListener(this);
        imgSeeds.setOnClickListener(this);
        imgServers.setOnClickListener(this);
        imgPacks.setOnClickListener(this);
        imgTextures.setOnClickListener(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        tvVersion.setText(BuildConfig.VERSION_NAME);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(MainActivity.this, CategorySubActivity.class);
        switch (v.getId()) {
            case R.id.imgMaps:
                for (int i = 0; i < datas.size(); i++) {
                    item = datas.get(i);
                    type = item.get(CategoryObj.id, "");
                    if (type.equals("maps"))
                        intent.putExtra("data", item);
                }
                break;

            case R.id.imgBuilDing:
                for (int i = 0; i < datas.size(); i++) {
                    item = datas.get(i);
                    type = item.get(CategoryObj.id, "");
                    if (type.equals("builder"))
                        intent.putExtra("data", item);
                }
                break;

            case R.id.imgMods:
                for (int i = 0; i < datas.size(); i++) {
                    item = datas.get(i);
                    type = item.get(CategoryObj.id, "");
                    if (type.equals("mods"))
                        intent.putExtra("data", item);
                }
                break;

            case R.id.imgSeeds:
                for (int i = 0; i < datas.size(); i++) {
                    item = datas.get(i);
                    type = item.get(CategoryObj.id, "");
                    if (type.equals("seeds"))
                        intent.putExtra("data", item);
                }
                break;

            case R.id.imgServers:
                for (int i = 0; i < datas.size(); i++) {
                    item = datas.get(i);
                    type = item.get(CategoryObj.id, "");
                    if (type.equals("servers"))
                        intent.putExtra("data", item);
                }
                break;

            case R.id.imgPacks:
                for (int i = 0; i < datas.size(); i++) {
                    item = datas.get(i);
                    type = item.get(CategoryObj.id, "");
                    if (type.equals("packs"))
                        intent.putExtra("data", item);
                }
                break;

            case R.id.imgTextures:
                for (int i = 0; i < datas.size(); i++) {
                    item = datas.get(i);
                    type = item.get(CategoryObj.id, "");
                    if (type.equals("textures"))
                        intent.putExtra("data", item);
                }
                break;

        }
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("SonLv", "onDestroy: ");
        if (!SettingSuppost.isCheckSub(this)) return;
        if (!SettingSuppost.isCheckProduct(this)) return;
        SettingSuppost.isHideShowApp(this);
    }
}
