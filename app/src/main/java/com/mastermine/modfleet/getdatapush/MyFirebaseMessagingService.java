package com.mastermine.modfleet.getdatapush;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.mastermine.modfleet.updatecontent.UpdateWorker;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.d("SonLv", "onMessageReceived");
        UpdateWorker.start(this);
    }

}
