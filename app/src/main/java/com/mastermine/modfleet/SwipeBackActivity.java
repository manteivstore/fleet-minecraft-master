package com.mastermine.modfleet;

import android.graphics.Color;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;

import com.mastermine.modfleet.Net.TaskType;
import com.mastermine.modfleet.action.SwipeBackLayout;
import com.telpoo.frame.ui.BetaBaseActionbarActivity;


import static androidx.core.view.ViewCompat.animate;


public class SwipeBackActivity extends BetaBaseActionbarActivity implements TaskType {

    private SwipeBackLayout swipeBackLayout;
    public Toolbar toolbar;
    public View progress;

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(R.layout.activity_swipe_back);
        swipeBackLayout = findViewById(R.id.swipe_layout);
        View view = LayoutInflater.from(this).inflate(layoutResID, null);
        toolbar = view.findViewById(R.id.toolbar);
        progress = view.findViewById(R.id.progressBar);
        if (toolbar != null) {
            toolbar.setTitleTextColor(0xFFFFFFFF);
            toolbar.setSubtitleTextColor(0xFFFFFFFF);
            toolbar.setNavigationIcon(R.drawable.btn_back);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }
        swipeBackLayout.addView(view);
        initSwipeBack();

        MyApplication.getInstance().trackScreenView(getClass().getSimpleName());

    }

    @Override
    public void setTitle(int titleId) {
        getSupportActionBar().setTitle(titleId);
    }

    @Override
    public void setTitle(CharSequence title) {
        getSupportActionBar().setTitle(title);
    }

    public void hindeProgress() {
        if (progress == null) return;
        progress.setVisibility(View.GONE);
    }

    public void showProgress() {
        if (progress == null) return;
        progress.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }


    void initSwipeBack() {
        swipeBackLayout = findViewById(R.id.swipe_layout);
        swipeBackLayout.setDragEdge(SwipeBackLayout.DragEdge.LEFT);
        swipeBackLayout.setEnablePullToBack(false);

        findViewById(R.id.touch_swipe_enable).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    swipeBackLayout.setLocked(true);
                    swipeBackLayout.setEnablePullToBack(true);
                    if (mOnSwipe != null) mOnSwipe.onSwipe(true);
                }
                return false;
            }
        });
        findViewById(R.id.touch_swipe_disable).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    swipeBackLayout.setLocked(false);
                    swipeBackLayout.setEnablePullToBack(false);
                    if (mOnSwipe != null) mOnSwipe.onSwipe(false);
                }
                return false;
            }
        });

        findViewById(R.id.swipe_bg).setBackgroundColor(Color.parseColor("#bb000000"));
        swipeBackLayout.setOnSwipeBackListener(new SwipeBackLayout.SwipeBackListener() {
            @Override
            public void onViewPositionChanged(float fractionAnchor, float fractionScreen) {
                animate(findViewById(R.id.swipe_bg)).alpha(1 - fractionScreen).setDuration(0).start();
                if (fractionScreen <= 0)
                    if (mOnSwipe != null) mOnSwipe.onSwipe(false);
            }
        });
    }

    public void setEnablePullToBack(boolean b) {
        swipeBackLayout.setLocked(b);
        swipeBackLayout.setEnablePullToBack(b);
        findViewById(R.id.touch_swipe_enable).setVisibility(b ? View.VISIBLE : View.GONE);
    }

    public SwipeBackLayout getSwipeBackLayout() {
        return swipeBackLayout;
    }

    OnSwipe mOnSwipe;

    public void setOnSwipe(OnSwipe onSwipe) {
        mOnSwipe = onSwipe;
    }

    public interface OnSwipe {
        void onSwipe(boolean swipe);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Log.e("onOptionsItemSelected", "call finish activity from home button");
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
