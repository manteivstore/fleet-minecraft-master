package com.mastermine.modfleet;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.doubleclick.PublisherAdView;
import com.mastermine.modfleet.Net.SPRsupport;
import com.mastermine.modfleet.Net.TaskNet;
import com.mastermine.modfleet.adapter.CommentAdapter;
import com.mastermine.modfleet.adapter.SmallIteamAdapter;
import com.mastermine.modfleet.ads.AdsLoader;
import com.mastermine.modfleet.ads.AdsSuppost;
import com.mastermine.modfleet.ads.listener;
import com.mastermine.modfleet.onesignal.EventFirebase;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.mastermine.modfleet.action.DialogsSupport;
import com.mastermine.modfleet.action.Listener;
import com.mastermine.modfleet.action.PermissionSupport;
import com.mastermine.modfleet.bean.CategoryObj;
import com.mastermine.modfleet.bean.ItemObj;
import com.telpoo.frame.model.BaseModel;
import com.telpoo.frame.object.BaseObject;
import com.telpoo.frame.utils.KeyboardSupport;


import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.concurrent.Executors;

public class DetailItemActivity extends SwipeBackActivity {

    BaseObject object = new BaseObject();
    ImageView img;
    TextView tvDescription, tvVersion;
    TextView tvLike;
    TextView tvView;
    TextView tvDownload;
    TextView tvComment;
    TextView tvSetLike;
    LinearLayout lnClickLike;
    LinearLayout lnComment;
    ImageView imgLike;

    View btnDownload;
    String category = "";
    TextView tvType;
    TextView tvDownloadFree;
    String type = "";
    String post_id_download_view = "";
    String post_id = "post_id";
    boolean isShowAdsSuccess = false;
    boolean isCheck;
    int post_like = 0;
    private PublisherAdView mPublisherAdView;
    private RelativeLayout rlBanner;

    PopupWindow popupWindow;
    CommentAdapter adapter;
    int page = 1;
    int limit = 10;
    Boolean cancelRequest = true;
    String comment;

    ArrayList<BaseObject> list;
    String zipcode = "";
    RecyclerView listItemSmall;
    SmallIteamAdapter smallIteamAdapter;

    BaseModel baseModel = new BaseModel() {
        @Override
        public void onSuccess(int taskType, Object data, String msg) {
            super.onSuccess(taskType, data, msg);
            closeProgressDialog();
            switch (taskType) {
                case TASK_UPDATELIKE:
                    SPRsupport.saveIsLike(post_id, DetailItemActivity.this, isCheck);
                    break;
                case TASK_DOWNLOAD:
                    break;
                case TASK_VIEW:
                    break;
                case TASK_GET_COMMENT:
                    list = (ArrayList<BaseObject>) data;
                    tvComment.setText(list.size() + "");
                    if (page == 0) adapter.setData(list);
                    else adapter.addData(list);
                    if (list.size() == limit) {
                        page += 1;
                        cancelRequest = true;
                    }
                    break;
                case TASK_POST_COMMENT:
                    getComment();
                    break;
                case TASK_SMALL_LIST:
                    list = (ArrayList<BaseObject>) data;
                    if (page == 0) smallIteamAdapter.setData(list);
                    else smallIteamAdapter.addData(list);
                    if (list.size() == limit) {
                        page += 1;
                        cancelRequest = true;
                    }
                    break;
            }
        }

        @Override
        public void onFail(int taskType, String msg) {
            super.onFail(taskType, msg);
            closeProgressDialog();
            switch (taskType) {
                case TASK_UPDATELIKE:
                    Toast.makeText(DetailItemActivity.this, msg, Toast.LENGTH_LONG).show();
                    break;
                case TASK_DOWNLOAD:
                    Toast.makeText(DetailItemActivity.this, msg, Toast.LENGTH_LONG).show();
                    break;
                case TASK_VIEW:
                    Toast.makeText(DetailItemActivity.this, msg, Toast.LENGTH_LONG).show();
                    break;
                case TASK_GET_COMMENT:
                    Toast.makeText(DetailItemActivity.this, msg, Toast.LENGTH_LONG).show();
                    break;
                case TASK_POST_COMMENT:
                    Toast.makeText(DetailItemActivity.this, msg, Toast.LENGTH_LONG).show();
                    break;
                case TASK_SMALL_LIST:
                    Toast.makeText(DetailItemActivity.this, msg, Toast.LENGTH_LONG).show();
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail);

        initView();
        initData();

    }

    private void initView() {
        popupWindow = new PopupWindow(this);
        img = findViewById(R.id.img);
        tvDescription = findViewById(R.id.tvDescription);
        tvVersion = findViewById(R.id.tvVersion);
        tvLike = findViewById(R.id.tvLike);
        tvView = findViewById(R.id.tvView);
        tvDownload = findViewById(R.id.tvDownload);
        tvComment = findViewById(R.id.tvComment);
        lnClickLike = findViewById(R.id.lnClickLike);
        tvSetLike = findViewById(R.id.tvSetLike);
        imgLike = findViewById(R.id.imgLike);
        lnComment = findViewById(R.id.lnComment);
        mPublisherAdView = findViewById(R.id.mPublisherAdView);
        rlBanner = findViewById(R.id.rlBanner);
        rlBanner.setVisibility(View.GONE);

        btnDownload = findViewById(R.id.btnDownload);
        tvType = findViewById(R.id.tvType);
        tvDownloadFree = findViewById(R.id.tvDownloadFree);

        smallIteamAdapter = new SmallIteamAdapter(this);
        listItemSmall = findViewById(R.id.listItemSmall);
        listItemSmall.setNestedScrollingEnabled(false);
        listItemSmall.setHasFixedSize(true);
        listItemSmall.setItemAnimator(new DefaultItemAnimator());
        listItemSmall.setLayoutManager(new LinearLayoutManager(this));
        listItemSmall.setAdapter(smallIteamAdapter);
        showProgressDialog(this);
    }

    private void initData() {
        object = getIntent().getParcelableExtra("data");
        category = getIntent().getStringExtra("category");
        type = getIntent().getStringExtra("type");
        post_like = Integer.parseInt(object.get(ItemObj.post_like, ""));
        post_id_download_view = object.get(ItemObj.id, "");
        tvLike.setText(object.get(ItemObj.post_like, ""));
        tvView.setText(object.get(ItemObj.views, ""));
        tvDownload.setText(object.get(ItemObj.download, ""));
        isCheck = SPRsupport.isLike(object.get(ItemObj.id, ""), this);

        if (!isCheck) {
            lnClickLike.setBackground(getResources().getDrawable(R.drawable.custom_unlike));
            tvSetLike.setText("Like this");
            tvSetLike.setTextColor(getResources().getColor(R.color.black_overlay));
            imgLike.setImageDrawable(getResources().getDrawable(R.drawable.ic_like_black));
        } else {
            post_like = post_like + 1;
            tvLike.setText(post_like + "");
            lnClickLike.setBackground(getResources().getDrawable(R.drawable.custom_like));
            tvSetLike.setText("Liked!");
            tvSetLike.setTextColor(getResources().getColor(R.color.white));
            imgLike.setImageDrawable(getResources().getDrawable(R.drawable.ic_unlike));
        }

        pushDownloadView(post_id_download_view, "view", TASK_VIEW);

        tvType.setText(object.get(CategoryObj.name, ""));

        ImageLoader.getInstance().displayImage(object.get(CategoryObj.thumbnail, ""), img, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                ImageLoader.getInstance().displayImage(object.get(ItemObj.image, ""), img, MyApplication.getImageOption());
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                ImageLoader.getInstance().displayImage(object.get(ItemObj.image, ""), img, MyApplication.getImageOption());
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
            }
        });
        tvDescription.setText(object.get(ItemObj.description, ""));
        tvVersion.setText("Version: " + object.get(ItemObj.version, ""));

        btnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventFirebase.getInstall(DetailItemActivity.this).logEvent("Click_Download");
                checkDownload();
            }
        });

        if (object.get(ItemObj.file, "").isEmpty()) btnDownload.setVisibility(View.GONE);

        AdsLoader.getInstall(this).showBanner(mPublisherAdView, 0, new listener.adsLoadShowBanner() {
            @Override
            public void onAdLoaded() {
                rlBanner.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadFailAds() {
                rlBanner.setVisibility(View.GONE);
            }

            @Override
            public void onAdClosed() {
                rlBanner.setVisibility(View.GONE);
            }
        });

        lnClickLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                post_id = object.get(ItemObj.id, "");
                isCheck = SPRsupport.isLike(post_id, DetailItemActivity.this);
                updateLike(post_id);
            }
        });
        adapter = new CommentAdapter(this);
        loadComment();
        lnComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callPopup();
            }

        });
        loadSmallList();
    }

    private void callPopup() {
        LayoutInflater layoutInflater = (LayoutInflater) getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = layoutInflater.inflate(R.layout.popup_window_cmt, null);
        popupWindow = new PopupWindow(popupView,
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, false);
        popupWindow.setTouchable(true);
        popupWindow.setFocusable(true);

        popupWindow.showAtLocation(popupView, Gravity.BOTTOM, 0, 0);
        RecyclerView rcView = popupView.findViewById(R.id.rcView);
        TextView tvtypeComt = popupView.findViewById(R.id.tvtypeComt);
        ImageView imgBack = popupView.findViewById(R.id.imgBack);
        tvtypeComt.setText("Comment");
        EditText edtCmt = popupView.findViewById(R.id.edtCmt);
        ImageView imgSendCmt = popupView.findViewById(R.id.imgSendCmt);
        edtCmt.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        rcView.setHasFixedSize(true);
        rcView.setItemAnimator(new DefaultItemAnimator());
        rcView.setLayoutManager(new LinearLayoutManager(this));
        rcView.setAdapter(adapter);
        rcView.setOnTouchListener((view1, motionEvent) -> {
            switch (motionEvent.getAction()) {
                case MotionEvent.ACTION_DOWN:
                case MotionEvent.ACTION_POINTER_UP:
                case MotionEvent.ACTION_POINTER_DOWN:
                case MotionEvent.ACTION_MOVE:
                    KeyboardSupport.hideKeyboard(this, edtCmt);
                    break;
            }
            return false;
        });

        edtCmt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                comment = editable.toString();
                if (!comment.isEmpty()) imgSendCmt.setVisibility(View.VISIBLE);
                else imgSendCmt.setVisibility(View.GONE);
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                KeyboardSupport.hideKeyboard(DetailItemActivity.this, edtCmt);
                popupWindow.dismiss();
            }
        });

        imgSendCmt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comment = edtCmt.getText().toString();
                if (comment.trim().length() == 0) return;
                String client_id = Settings.Secure.getString(getContext().getContentResolver(), Settings.Secure.ANDROID_ID);
                postComment(post_id_download_view, "", client_id, comment);
                edtCmt.setText("");
            }
        });

    }

    public void loadComment() {
        if (!cancelRequest) return;
        cancelRequest = false;
        getComment();
    }

    public void updateLike(String id) {
        if (!isCheck) {
            updateLikeUnLike(id, "like");
            lnClickLike.setBackground(getResources().getDrawable(R.drawable.custom_like));
            tvSetLike.setText("Liked!");
            tvSetLike.setTextColor(getResources().getColor(R.color.white));
            imgLike.setImageDrawable(getResources().getDrawable(R.drawable.ic_unlike));
            post_like = post_like + 1;
            tvLike.setText(post_like + "");
            isCheck = true;
        } else {
            updateLikeUnLike(id, "unlike");
            lnClickLike.setBackground(getResources().getDrawable(R.drawable.custom_unlike));
            tvSetLike.setText("Like this");
            tvSetLike.setTextColor(getResources().getColor(R.color.black_overlay));
            imgLike.setImageDrawable(getResources().getDrawable(R.drawable.ic_like_black));
            if (post_like > 0) post_like = post_like - 1;
            tvLike.setText(post_like + "");
            isCheck = false;
        }
    }

    private void updateLikeUnLike(String id, String type) {
        BaseObject baseObject = new BaseObject();
        baseObject.set("reaction", type);
        baseObject.set("pid ", id);
        baseObject.removeEmpty();
        TaskNet taskNet = new TaskNet(baseModel, TASK_UPDATELIKE, this);
        Log.d("ducqv", "baseObject: " + baseObject.toJson());
        taskNet.setTaskParram("parram", baseObject);
        taskNet.exe();
    }

    private void getComment() {
        BaseObject object = new BaseObject();
        object.set("pid", post_id_download_view);
        object.set("per_page", limit);
        object.set("page", page);
        TaskNet taskNet = new TaskNet(baseModel, TASK_GET_COMMENT, this);
        taskNet.setTaskParram("parram", object);
        taskNet.exe();
    }

    public void loadSmallList() {
        BaseObject objectSmallList = new BaseObject();
        object.set("cat", SPRsupport.getCatId(this));
        object.set("per_page", limit);
        object.set("page", page);
        TaskNet taskNet = new TaskNet(baseModel, TASK_SMALL_LIST, this);
        taskNet.setTaskParram("parram", objectSmallList);
        taskNet.exe();
    }

    private void pushDownloadView(String id, String postcount, int task_type) {
        BaseObject baseObject = new BaseObject();
        baseObject.set("postcount", postcount);
        baseObject.set("pid ", id);
        baseObject.removeEmpty();
        TaskNet taskNet = new TaskNet(baseModel, task_type, this);
        taskNet.setTaskParram("parram", baseObject);
        taskNet.exe();
    }

    private void postComment(String id, String parent_id, String client_id, String comment) {
        BaseObject baseObject = new BaseObject();
        baseObject.set("pid", id);
        baseObject.set("parent_id", parent_id);
        baseObject.set("client_id", client_id);
        baseObject.set("content", comment);
        TaskNet taskNet = new TaskNet(baseModel, TASK_POST_COMMENT, this);
        taskNet.setTaskParram("parram", baseObject);
        taskNet.exe();
    }

    public void checkDownload() {
        if (AdsSuppost.getInstall(this).isAPK()
                && getPackageManager().getLaunchIntentForPackage(SPRsupport.getIdAppApk(this)) == null) {
            startDownload();
            return;
        }

        if (AdsSuppost.getInstall(this).isIAP()) {
            EventFirebase.getInstall(DetailItemActivity.this).logEvent("isSubSuccess");
            Log.d("SonLv", "isSubSuccess: ");
            if (MyApplication.getInstance().isSubSuccess()) {
                startDownload();
                return;
            }

            DialogsSupport.showDialogIAP(this, new Listener.OnDialogYesNoListener() {
                @Override
                public void onYesClick() {
                    MyApplication.getInstance().trackEvent("DialogIAP->Purchases", category, type);
                    EventFirebase.getInstall(DetailItemActivity.this).logEvent("DialogIAP->Purchases");

                    MyApplication.getInstance().processIAP(DetailItemActivity.this, new Listener.OnPurchasesListener() {
                        @Override
                        public void onSuccess() {
                            startDownload();
                        }

                        @Override
                        public void onError() {
                            showToast("Error Billing!");
                        }
                    });

                }

                @Override
                public void onNoClick() {
                    MyApplication.getInstance().trackEvent("DialogIAP->Free", category, type);
                    EventFirebase.getInstall(DetailItemActivity.this).logEvent("DialogIAP->Free");
                    if (isShowAdsSuccess) startDownload();
                    else showDialogUnLog();
                }
            });
            return;
        }

        if (AdsSuppost.getInstall(this).isProduct()) {
            EventFirebase.getInstall(DetailItemActivity.this).logEvent("isProduct");
            Log.d("SonLv", "isProduct: ");
            if (MyApplication.getInstance().isPurchaseSuccess()) {
                startDownload();
                return;
            }

            DialogsSupport.showDialogIAP(this, new Listener.OnDialogYesNoListener() {
                @Override
                public void onYesClick() {
                    MyApplication.getInstance().trackEvent("DialogIAP->Purchases", category, type);
                    EventFirebase.getInstall(DetailItemActivity.this).logEvent("DialogIAP->Purchases");

                    MyApplication.getInstance().processProductIAP(DetailItemActivity.this,
                            new Listener.OnPurchasesListener() {
                                @Override
                                public void onSuccess() {
                                    startDownload();
                                }

                                @Override

                                public void onError() {
                                    showToast("Error Billing!");
                                }
                            });

                }

                @Override
                public void onNoClick() {
                    MyApplication.getInstance().trackEvent("DialogIAP->Free", category, type);
                    EventFirebase.getInstall(DetailItemActivity.this).logEvent("DialogIAP->Free");
                    if (isShowAdsSuccess) startDownload();
                    else showDialogUnLog();
                }
            });
            return;

        }

        if (isShowAdsSuccess) {
            startDownload();
            return;
        }
        if (AdsSuppost.getInstall(this).isAds()) {
            showDialogUnLog();
            return;
        }
        startDownload();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("billingClient", "onActivityResult: " + resultCode);

        if (resultCode != RESULT_CANCELED) return;
    }


    public void showDialogUnLog() {
        //Bạn cần 50 coin để tải xuống. Xem video nhận 50 coin miễn phí
        MyApplication.getInstance().trackEvent("Unlock", category, type);
        EventFirebase.getInstall(DetailItemActivity.this).logEvent("Unlock");

        DialogsSupport.showDialogYesNo(this, "Watch", "Close", null,
                "You need 50 coins to download. Watch videos get 50 coins free!", new Listener.OnDialogYesNoListener() {
                    @Override
                    public void onYesClick() {
                        MyApplication.getInstance().trackEvent("Unlock->Watch", category, type);
                        EventFirebase.getInstall(DetailItemActivity.this).logEvent("Unlock->Watch");
                        AdsLoader.getInstall(DetailItemActivity.this).showVideoAdmob(new listener.adsLoadShowVideos() {
                            @Override
                            public void onAdLoaded(String type) {

                            }

                            @Override
                            public void onLoadFailAds(String type) {
                                startDownload();
                            }

                            @Override
                            public void onAdClosed(String type) {
                                startDownload();
                            }
                        });
                    }

                    @Override
                    public void onNoClick() {
                        MyApplication.getInstance().trackEvent("Unlock->Close", category, type);
                        EventFirebase.getInstall(DetailItemActivity.this).logEvent("Unlock->Close");
                    }
                });

    }

    public void startDownload() {
        MyApplication.getInstance().trackEvent("Download", category, type);
        EventFirebase.getInstall(DetailItemActivity.this).logEvent("Download_File");
        if (!PermissionSupport.getInstall(this).requestPermissionStore()) return;

        DownloadFileAsyn downloadFileAsyn = new DownloadFileAsyn();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            downloadFileAsyn.executeOnExecutor(Executors.newFixedThreadPool(1));
        else downloadFileAsyn.execute();
        showToast("Start download!");

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (PermissionSupport.getInstall(this).hasPermissionGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE))
            startDownload();
    }

    private class DownloadFileAsyn extends AsyncTask<Void, String, Void> {
        private ProgressDialog mProgressDialog;

        @Override
        protected Void doInBackground(Void... params) {
            int count;
            URL url;
            OutputStream output;
            try {
                if (AdsSuppost.getInstall(DetailItemActivity.this).isAPK()
                        && getPackageManager().getLaunchIntentForPackage(SPRsupport.getIdAppApk(DetailItemActivity.this)) == null) {
                    url = new URL(SPRsupport.getLinkApk(DetailItemActivity.this));
                    output = new FileOutputStream(getFileNameApk());
                } else {
                    url = new URL(object.get(ItemObj.file, ""));
                    output = new FileOutputStream(getFileName());
                }

                URLConnection conexion = url.openConnection();
                conexion.connect();
                int lenghtOfFile = conexion.getContentLength();
                InputStream input = new BufferedInputStream(url.openStream());
                byte data[] = new byte[1024];
                long total = 0;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));
                    output.write(data, 0, count);
                }
                output.flush();
                output.close();
                input.close();
            } catch (Exception e) {
                MyApplication.getInstance().trackException(e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (AdsSuppost.getInstall(DetailItemActivity.this).isAPK()
                    && getPackageManager().getLaunchIntentForPackage(SPRsupport.getIdAppApk(DetailItemActivity.this)) == null) {
                EventFirebase.getInstall(DetailItemActivity.this).logEvent("Download_Success_Apk");
                openFileApk();
                mProgressDialog.dismiss();
                Log.d("SonLv", "DownloadDone Apk");

            } else {
                EventFirebase.getInstall(DetailItemActivity.this).logEvent("Download_Success_Link");
                mProgressDialog.dismiss();
                Log.d("SonLv", "DownloadDone Link");
                DialogsSupport.showDialogYesNo(DetailItemActivity.this, "Open", "Close", "Download success!", null, new Listener.OnDialogYesNoListener() {
                    @Override
                    public void onYesClick() {
                        MyApplication.getInstance().trackEvent("Download->Open", category, type);
                        EventFirebase.getInstall(DetailItemActivity.this).logEvent("Download->Open");
                        openFile();
                    }

                    @Override
                    public void onNoClick() {
                        MyApplication.getInstance().trackEvent("Download->Close", category, type);
                        EventFirebase.getInstall(DetailItemActivity.this).logEvent("Download->Close");
                    }
                });
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(new ContextThemeWrapper(DetailItemActivity.this, android.R.style.Theme_Holo_Light_Dialog));
            mProgressDialog.setMessage("Downloading...");
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            mProgressDialog.setProgress(Integer.parseInt(values[0]));
        }

    }

    public String getFileName() {
        try {
            String path[] = object.get(ItemObj.file, "").replaceAll("%2F", "/").split("/");
            String file[] = path[path.length - 1].split("\\?");
            return Environment.getExternalStorageDirectory() + "/" + file[0];
        } catch (Exception e) {
            MyApplication.getInstance().trackException(e);
        }
        return "file.tmp";
    }

    protected void openFile() {
        try {
            String fileName = getFileName();
            Log.d("SonLv", fileName);
            Uri uri = FileProvider.getUriForFile(this, getPackageName(), new File(fileName));

            Intent install = new Intent(Intent.ACTION_VIEW);
            install.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            install.setData(uri);
            startActivity(install);

        } catch (Exception e) {
            DialogsSupport.showDialogYesNo(this, "User manual", "Close", null, "Minecraft not install", new Listener.OnDialogYesNoListener() {
                @Override
                public void onYesClick() {
                    try {
                        Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://letitiagemma.xyz/policy/hdsd-app.pdf"));
                        startActivity(myIntent);
                    } catch (ActivityNotFoundException ex) {
                    }
                }

                @Override
                public void onNoClick() {

                }
            });
            MyApplication.getInstance().trackException(e);
        }

    }

    public String getFileNameApk() {
        return Environment.getExternalStorageDirectory() + "/" + "Minecraft Trial" + ".apk";
    }

    protected void openFileApk() {
        String fileNameApk = getFileNameApk();
        Log.d("SonLv", "fileName 1: " + fileNameApk);
        Intent install = new Intent(Intent.ACTION_VIEW);
        install.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_GRANT_READ_URI_PERMISSION);
        try {
            install.setDataAndType(Uri.fromFile(new File(fileNameApk)), "application/vnd.android.package-archive");
            startActivity(install);
        } catch (Exception e) {
            install.setDataAndType(FileProvider.getUriForFile(this, getPackageName(), new File(fileNameApk)), "application/vnd.android.package-archive");
            startActivity(install);
        }
        Log.d("SonLv", "fileNameApk" + fileNameApk);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (type.equals("notifi"))
            startActivity(new Intent(this, MainActivity.class));
    }
}
