# Chỉ định không để xác minh trước các tệp lớp được xử lý.
# điều này là không cần thiết nên có thể sử dụng thuộc tính này để tắt nó đi
-dontpreverify

# Đơn giản hóa số học mà Dalvik 1.0 và 1.5 không thể xử lý
-optimizations !code/simplification/arithmetic,!code/simplification/cast,!field/*,!class/merging/*
-optimizations !method/inlining/*

# Số lần tối ưu hóa dc thực hiện
-optimizationpasses 5
## Bỏ qua các class không phải là public trong thư viện.
## để tăng tốc xử lý và giảm thiểu sử dụng bộ nhớ của ProGuard
#-skipnonpubliclibraryclasses

# Khi ProGuard thì các tên trường sẽ bị xáo trộn kết hợp các kí tự hoa thường.
# Nếu dùng thuộc tính này thì sẽ ngăn không cho xáo trộn trên
-dontusemixedcaseclassnames

# khi bạn dùng proguard để " xáo trộn code " , tất nhiên sẽ có
# 1 số ngoại lệ , dòng này sẽ in ra toàn bộ dấu vết của ngoại lệ này lên màn hình console
# còn nếu không có thì 1 là chỉ có 1 hộp thoại thông báo lỗi , hoặc không có thông báo gì
-verbose

# Đổi tên tất cả các class , package  thành 1 ký tự đơn như a, b, c...
#-repackageclasses ''

 #Xóa tất cả các Log.v(), Log.i()...
 -assumenosideeffects class android.util.Log {
     public static *** d(...);
     public static *** v(...);
     public static *** w(...);
     public static *** e(...);
     public static *** i(...);
 }

-dontwarn com.google.android.gms.common.GooglePlayServicesUtil
-dontwarn com.google.android.gms.common.GoogleApiAvailability
-dontwarn com.google.android.gms.internal.zzbif
-dontwarn com.google.android.gms.internal.zzbig
-dontwarn com.google.android.gms.gcm.zza
-dontwarn com.google.android.gms.internal.measurement.zzee
-dontwarn com.google.android.gms.internal.measurement.zzei
-dontwarn com.google.android.gms.internal.measurement.zzgf
-dontwarn com.google.android.gms.internal.measurement.zzgl
-dontwarn com.google.android.gms.internal.measurement.zzjv
-dontwarn com.google.android.gms.internal.measurement.zzjy
-dontwarn com.google.android.gms.internal.measurement.zzjz
-dontwarn com.google.android.gms.internal.measurement.zzka
-dontwarn com.google.android.gms.internal.measurement.zzkb
-dontwarn com.google.android.gms.internal.measurement.zzkc
-dontwarn com.google.android.gms.internal.measurement.zzkd
-dontwarn com.google.android.gms.internal.measurement.zzke
-dontwarn com.google.android.gms.internal.measurement.zzkf
-dontwarn com.google.android.gms.internal.measurement.zzkg
-dontwarn com.google.android.gms.internal.measurement.zzkh
-dontwarn com.google.android.gms.internal.measurement.zzki
-dontwarn com.google.android.gms.internal.measurement.zzkj
-dontwarn com.google.android.gms.internal.measurement.zzkk
-dontwarn com.google.android.gms.internal.measurement.zzkl
-dontwarn com.google.android.gms.internal.measurement.zzkm
-dontwarn com.google.android.gms.internal.measurement.zzkn
-dontwarn com.google.android.gms.internal.measurement.zzabj
-dontwarn android.support.v4.media.AudioAttributesCompat
-dontwarn android.support.v4.media.AudioAttributesImplBase
-dontwarn android.support.v4.media.AudioAttributesImplApi21

-dontwarn com.google.android.gms.common.internal.safeparcel.SafeParcelable
-dontwarn javax.annotation.Nullable
-dontwarn org.conscrypt.OpenSSLProvider
-dontwarn org.conscrypt.Conscrypt
-dontwarn javax.annotation.ParametersAreNonnullByDefault


-dontwarn org.apache.commons.**
-keep class org.apache.http.** { *; }
-dontwarn org.apache.http.**
-dontwarn com.google.android.exoplayer2.**

-keep class com.android.vending.billing.**
-keep public class * extends androidx.versionedparcelable.VersionedParcelable {
  <init>();
}